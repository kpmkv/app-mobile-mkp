package in.setone.valent.mkvbookkatalogue.room.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import in.setone.valent.mkvbookkatalogue.data.lks.parcelablelks.ParcelLks;

@Entity(tableName = "tb_book_saved")
public class BookModel {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    @Expose
    @SerializedName("id")
    protected int id;

    @ColumnInfo(name = "nama")
    @Expose
    @SerializedName("nama")
    protected String nama;

    @ColumnInfo(name = "isbn")
    @Expose
    @SerializedName("isbn")
    protected String isbn;

    @ColumnInfo(name = "type")
    @Expose
    @SerializedName("type")
    protected String type;

    @ColumnInfo(name = "kategori")
    @Expose
    @SerializedName("kategori")
    protected String kategori;

    @ColumnInfo(name = "kurikulum")
    @Expose
    @SerializedName("kurikulum")
    protected String kurikulum;

    @ColumnInfo(name = "jenjang")
    @Expose
    @SerializedName("jenjang")
    protected String jenjang;

    @ColumnInfo(name = "kelas")
    @Expose
    @SerializedName("kelas")
    protected int kelas;

    @ColumnInfo(name = "semester")
    @Expose
    @SerializedName("semester")
    protected int semester;

    @ColumnInfo(name = "jumlah_halaman")
    @Expose
    @SerializedName("jumlah_halaman")
    protected int jumlah_halaman;

    @ColumnInfo(name = "pengarang")
    @Expose
    @SerializedName("pengarang")
    protected String pengarang;

    @ColumnInfo(name = "editor")
    @Expose
    @SerializedName("editor")
    protected String editor;

    @ColumnInfo(name = "overview")
    @Expose
    @SerializedName("overview")
    protected String overview;

    @ColumnInfo(name = "tanggal_terbit")
    @Expose
    @SerializedName("tanggal_terbit")
    protected String tanggal_terbit;

    @ColumnInfo(name = "path_foto")
    @Expose
    @SerializedName("path_foto")
    protected String path_foto;

    @ColumnInfo(name = "path_daftarisi")
    @Expose
    @SerializedName("path_daftarisi")
    protected String path_daftarisi;

    @ColumnInfo(name = "bahasa")
    @Expose
    @SerializedName("bahasa")
    protected String bahasa;

    @ColumnInfo(name = "penerbit")
    @Expose
    @SerializedName("penerbit")
    protected String penerbit;

    public BookModel(int id, String nama, String isbn, String type, String kategori, String kurikulum, String jenjang, int kelas, int semester, int jumlah_halaman, String pengarang, String editor, String overview, String tanggal_terbit, String path_foto, String path_daftarisi, String bahasa, String penerbit) {
        this.id = id;
        this.nama = nama;
        this.isbn = isbn;
        this.type = type;
        this.kategori = kategori;
        this.kurikulum = kurikulum;
        this.jenjang = jenjang;
        this.kelas = kelas;
        this.semester = semester;
        this.jumlah_halaman = jumlah_halaman;
        this.pengarang = pengarang;
        this.editor = editor;
        this.overview = overview;
        this.tanggal_terbit = tanggal_terbit;
        this.path_foto = path_foto;
        this.path_daftarisi = path_daftarisi;
        this.bahasa = bahasa;
        this.penerbit = penerbit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getKurikulum() {
        return kurikulum;
    }

    public void setKurikulum(String kurikulum) {
        this.kurikulum = kurikulum;
    }

    public String getJenjang() {
        return jenjang;
    }

    public void setJenjang(String jenjang) {
        this.jenjang = jenjang;
    }

    public int getKelas() {
        return kelas;
    }

    public void setKelas(int kelas) {
        this.kelas = kelas;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public int getJumlah_halaman() {
        return jumlah_halaman;
    }

    public void setJumlah_halaman(int jumlah_halaman) {
        this.jumlah_halaman = jumlah_halaman;
    }

    public String getPengarang() {
        return pengarang;
    }

    public void setPengarang(String pengarang) {
        this.pengarang = pengarang;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getTanggal_terbit() {
        return tanggal_terbit;
    }

    public void setTanggal_terbit(String tanggal_terbit) {
        this.tanggal_terbit = tanggal_terbit;
    }

    public String getPath_foto() {
        return path_foto;
    }

    public void setPath_foto(String path_foto) {
        this.path_foto = path_foto;
    }

    public String getPath_daftarisi() {
        return path_daftarisi;
    }

    public void setPath_daftarisi(String path_daftarisi) {
        this.path_daftarisi = path_daftarisi;
    }

    public String getBahasa() {
        return bahasa;
    }

    public void setBahasa(String bahasa) {
        this.bahasa = bahasa;
    }

    public String getPenerbit() {
        return penerbit;
    }

    public void setPenerbit(String penerbit) {
        this.penerbit = penerbit;
    }

    @Override
    public String toString() {
        return "ModelPaket{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                ", isbn='" + isbn + '\'' +
                ", type='" + type + '\'' +
                ", kategori='" + kategori + '\'' +
                ", kurikulum='" + kurikulum + '\'' +
                ", jenjang='" + jenjang + '\'' +
                ", kelas=" + kelas +
                ", semester=" + semester +
                ", jumlah_halaman=" + jumlah_halaman +
                ", pengarang='" + pengarang + '\'' +
                ", editor='" + editor + '\'' +
                ", overview='" + overview + '\'' +
                ", tanggal_terbit='" + tanggal_terbit + '\'' +
                ", path_foto='" + path_foto + '\'' +
                ", path_daftarisi='" + path_daftarisi + '\'' +
                ", bahasa='" + bahasa + '\'' +
                ", penerbit='" + penerbit + '\'' +
                '}';
    }
}
