package in.setone.valent.mkvbookkatalogue.ui.paket;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import in.setone.valent.mkvbookkatalogue.data.lks.RepositoryLks;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ListModelLks;
import in.setone.valent.mkvbookkatalogue.data.paket.RepositoryPaket;
import in.setone.valent.mkvbookkatalogue.data.paket.model.ListModelPaket;
import in.setone.valent.mkvbookkatalogue.root.handler.Result;

public class PaketViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private RepositoryPaket repositoryPaket = new RepositoryPaket();

    public PaketViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is notifications fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }

    public MutableLiveData<Result<ListModelPaket>> liveDataPaket() {
        return repositoryPaket.dataPaket();
    }

    public MutableLiveData<Result<ListModelPaket>> searchQueryPaket(String query){
        return repositoryPaket.resultSearchPaket(query);
    }
}