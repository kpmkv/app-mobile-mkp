package in.setone.valent.mkvbookkatalogue.data.lks.model;

import java.util.List;

public class ListModelLks {
    private List<ModelLks> listOfDataLks;

    public ListModelLks(List<ModelLks> listOfDataLks) {
        this.listOfDataLks = listOfDataLks;
    }

    public List<ModelLks> getDataLks() {
        return listOfDataLks;
    }

    public void setDataLks(List<ModelLks> listOfDataLks) {
        this.listOfDataLks = listOfDataLks;
    }

    @Override
    public String toString() {
        return "ListModelLks{" +
                "listOfDataLks=" + listOfDataLks +
                '}';
    }
}
