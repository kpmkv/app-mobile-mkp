package in.setone.valent.mkvbookkatalogue.ui.details.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.*;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import in.setone.valent.mkvbookkatalogue.BuildConfig;
import in.setone.valent.mkvbookkatalogue.R;

import java.util.List;

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.ViewHolder> {

    private List<String> data;

    public GridAdapter(List<String> data) {
//        Log.d("DataGridCount", String.valueOf(data.size()));
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image_view_item_gird);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final String url = data.get(position);
        final String url_image = BuildConfig.uriApi + url;

        Picasso.get().load(url_image).into(holder.imageView);

        final Dialog settingsDialog = new Dialog(holder.itemView.getContext());
        settingsDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        settingsDialog.setContentView(LayoutInflater.from(holder.itemView.getContext()).inflate(R.layout.zoom_image
                , null));
        settingsDialog.getWindow().setLayout(720, 1000);
        final ImageView im = settingsDialog.findViewById(R.id.zoomImage);
        final ImageView back = settingsDialog.findViewById(R.id.btnClose);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Picasso.get().load(url_image).into(im);
                settingsDialog.show();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsDialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        Log.d("DataGridCount", String.valueOf(data.size()));
        return data.size();
    }


}
