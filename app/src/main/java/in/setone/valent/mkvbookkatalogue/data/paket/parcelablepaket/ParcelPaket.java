package in.setone.valent.mkvbookkatalogue.data.paket.parcelablepaket;

import android.os.Parcel;
import android.os.Parcelable;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ModelLks;
import in.setone.valent.mkvbookkatalogue.data.paket.model.ModelPaket;

public class ParcelPaket extends ModelPaket implements Parcelable {
    public ParcelPaket(Parcel in) {
        super(
                in.readInt(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readInt(),
                in.readInt(),
                in.readInt(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString());
    }


    public static final Creator<ParcelPaket> CREATOR = new Creator<ParcelPaket>() {
        @Override
        public ParcelPaket createFromParcel(Parcel in) {
            return new ParcelPaket(in);
        }

        @Override
        public ParcelPaket[] newArray(int size) {
            return new ParcelPaket[size];
        }
    };

    public ParcelPaket(ModelPaket singleData) {
        super(
                singleData.getId(),
                singleData.getNama(),
                singleData.getIsbn(),
                singleData.getType(),
                singleData.getKategori(),
                singleData.getKurikulum(),
                singleData.getJenjang(),
                singleData.getKelas(),
                singleData.getSemester(),
                singleData.getJumlah_halaman(),
                singleData.getPengarang(),
                singleData.getEditor(),
                singleData.getOverview(),
                singleData.getTanggal_terbit(),
                singleData.getPath_foto(),
                singleData.getPath_daftarisi(),
                singleData.getBahasa(),
                singleData.getPenerbit());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.getId());
        dest.writeString(this.getNama());
        dest.writeString(this.getIsbn());
        dest.writeString(this.getType());
        dest.writeString(this.getKategori());
        dest.writeString(this.getKurikulum());
        dest.writeString(this.getJenjang());
        dest.writeInt(this.getKelas());
        dest.writeInt(this.getSemester());
        dest.writeInt(this.getJumlah_halaman());
        dest.writeString(this.getPengarang());
        dest.writeString(this.getEditor());
        dest.writeString(this.getOverview());
        dest.writeString(this.getTanggal_terbit());
        dest.writeString(this.getPath_foto());
        dest.writeString(this.getPath_daftarisi());
        dest.writeString(this.getBahasa());
        dest.writeString(this.getPenerbit());
    }
}
