package in.setone.valent.mkvbookkatalogue.network.model;

import com.google.gson.annotations.SerializedName;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ModelLks;

import java.util.List;

public class ResponseSearchLks {
    
    @SerializedName("status")
    private boolean status;
    @SerializedName("massage")
    private String massage;
    @SerializedName("query")
    private String query;
    @SerializedName("results")
    private List<ModelLks> results;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<ModelLks> getResults() {
        return results;
    }

    public void setResults(List<ModelLks> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "ResponseSearchLks{" +
                "status=" + status +
                ", massage='" + massage + '\'' +
                ", query='" + query + '\'' +
                ", results=" + results +
                '}';
    }
}
