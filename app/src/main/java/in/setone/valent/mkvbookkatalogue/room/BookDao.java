package in.setone.valent.mkvbookkatalogue.room;


import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.*;
import in.setone.valent.mkvbookkatalogue.room.model.BookModel;

import java.util.List;

@Dao
public interface BookDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(BookModel bookModel);

    @Query("DELETE FROM tb_book_saved")
    void deleteAll();

    @Delete
    void delete(BookModel bookModel);

    @Query("SELECT * from tb_book_saved ORDER BY id ASC")
    LiveData<List<BookModel>> getResults();

    //for widget
    @Query("SELECT * from tb_book_saved ORDER BY id ASC")
    List<BookModel> getList();

    //for contentprovider
    @Query("SELECT * from tb_book_saved ORDER BY id ASC")
    Cursor getCursor();

    @Query("SELECT * from tb_book_saved WHERE id = :id")
    Cursor loadCursor(int id);
}