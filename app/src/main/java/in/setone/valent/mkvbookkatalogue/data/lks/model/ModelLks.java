package in.setone.valent.mkvbookkatalogue.data.lks.model;

import com.google.gson.annotations.SerializedName;

public class ModelLks {
    @SerializedName("id")
    private int id;

    @SerializedName("nama")
    private String nama;

    @SerializedName("isbn")
    private String isbn;

    @SerializedName("type")
    private String type;

    @SerializedName("kategori")
    private String kategori;

    @SerializedName("kurikulum")
    private String kurikulum;

    @SerializedName("jenjang")
    private String jenjang;

    @SerializedName("kelas")
    private int kelas;

    @SerializedName("semester")
    private int semester;

    @SerializedName("jumlah_halaman")
    private int jumlah_halaman;

    @SerializedName("pengarang")
    private String pengarang;

    @SerializedName("editor")
    private String editor;

    @SerializedName("overview")
    private String overview;

    @SerializedName("tanggal_terbit")
    private String tanggal_terbit;

    @SerializedName("path_foto")
    private String path_foto;

    @SerializedName("path_daftarisi")
    private String path_daftarisi;

    @SerializedName("bahasa")
    private String bahasa;

    @SerializedName("penerbit")
    private String penerbit;

    public ModelLks(int id, String nama, String isbn, String type, String kategori, String kurikulum, String jenjang, int kelas, int semester, int jumlah_halaman, String pengarang, String editor, String overview, String tanggal_terbit, String path_foto, String path_daftarisi, String bahasa, String penerbit) {
        this.id = id;
        this.nama = nama;
        this.isbn = isbn;
        this.type = type;
        this.kategori = kategori;
        this.kurikulum = kurikulum;
        this.jenjang = jenjang;
        this.kelas = kelas;
        this.semester = semester;
        this.jumlah_halaman = jumlah_halaman;
        this.pengarang = pengarang;
        this.editor = editor;
        this.overview = overview;
        this.tanggal_terbit = tanggal_terbit;
        this.path_foto = path_foto;
        this.path_daftarisi = path_daftarisi;
        this.bahasa = bahasa;
        this.penerbit = penerbit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getKurikulum() {
        return kurikulum;
    }

    public void setKurikulum(String kurikulum) {
        this.kurikulum = kurikulum;
    }

    public String getJenjang() {
        return jenjang;
    }

    public void setJenjang(String jenjang) {
        this.jenjang = jenjang;
    }

    public int getKelas() {
        return kelas;
    }

    public void setKelas(int kelas) {
        this.kelas = kelas;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public int getJumlah_halaman() {
        return jumlah_halaman;
    }

    public void setJumlah_halaman(int jumlah_halaman) {
        this.jumlah_halaman = jumlah_halaman;
    }

    public String getPengarang() {
        return pengarang;
    }

    public void setPengarang(String pengarang) {
        this.pengarang = pengarang;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getTanggal_terbit() {
        return tanggal_terbit;
    }

    public void setTanggal_terbit(String tanggal_terbit) {
        this.tanggal_terbit = tanggal_terbit;
    }

    public String getPath_foto() {
        return path_foto;
    }

    public void setPath_foto(String path_foto) {
        this.path_foto = path_foto;
    }

    public String getPath_daftarisi() {
        return path_daftarisi;
    }

    public void setPath_daftarisi(String path_daftarisi) {
        this.path_daftarisi = path_daftarisi;
    }

    public String getBahasa() {
        return bahasa;
    }

    public void setBahasa(String bahasa) {
        this.bahasa = bahasa;
    }

    public String getPenerbit() {
        return penerbit;
    }

    public void setPenerbit(String penerbit) {
        this.penerbit = penerbit;
    }

    @Override
    public String toString() {
        return "ModelPaket{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                ", isbn='" + isbn + '\'' +
                ", type='" + type + '\'' +
                ", kategori='" + kategori + '\'' +
                ", kurikulum='" + kurikulum + '\'' +
                ", jenjang='" + jenjang + '\'' +
                ", kelas=" + kelas +
                ", semester=" + semester +
                ", jumlah_halaman=" + jumlah_halaman +
                ", pengarang='" + pengarang + '\'' +
                ", editor='" + editor + '\'' +
                ", overview='" + overview + '\'' +
                ", tanggal_terbit='" + tanggal_terbit + '\'' +
                ", path_foto='" + path_foto + '\'' +
                ", path_daftarisi='" + path_daftarisi + '\'' +
                ", bahasa='" + bahasa + '\'' +
                ", penerbit='" + penerbit + '\'' +
                '}';
    }
}
