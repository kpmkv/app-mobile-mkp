package in.setone.valent.mkvbookkatalogue.ui.lks;

import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.*;
import android.widget.SearchView;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.shimmer.ShimmerFrameLayout;
import in.setone.valent.mkvbookkatalogue.R;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ListModelLks;
import in.setone.valent.mkvbookkatalogue.room.AppDatabase;
import in.setone.valent.mkvbookkatalogue.room.BookDao;
import in.setone.valent.mkvbookkatalogue.root.handler.Result;
import in.setone.valent.mkvbookkatalogue.ui.lks.adapter.ListLksAdapter;

public class LksFragment extends Fragment {

    public static String KEY_SENDER_LKS = "SENDER_LKS";

    private LksViewModel lksViewModel;
    private String TAG = "LksFragment";
    private RecyclerView recyclerView;

    private RecyclerView.Adapter mAdapter;

    private RecyclerView.LayoutManager layoutManager;

    private ShimmerFrameLayout mShimmerViewContainer;

    private SearchView searchView;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        inflater.inflate(R.menu.options_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.search);
        searchView = (SearchView) MenuItemCompat.getActionView(menuItem);

        onSearchQuery();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        lksViewModel = ViewModelProviders.of(this).get(LksViewModel.class);
        View root = inflater.inflate(R.layout.fragment_lks, container, false);
        mShimmerViewContainer = root.findViewById(R.id.shimmer_view_container_lks);
        recyclerView = root.findViewById(R.id.list_lks);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        mShimmerViewContainer.startShimmerAnimation();

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);


        dummyAnimationLoading();

        getDataLks();

    }

    private void getDataLks() {
        lksViewModel.liveDataLks().observe(getActivity(), new Observer<Result<ListModelLks>>() {
            @Override
            public void onChanged(Result<ListModelLks> result) {
                ListModelLks modelLks = (ListModelLks) ((Result.Success) result).getData();
                mAdapter = new ListLksAdapter(modelLks.getDataLks(), getActivity());
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    private void onSearchQuery() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                lksViewModel.searchQueryLks(query).observe(getActivity(), new Observer<Result<ListModelLks>>() {
                    @Override
                    public void onChanged(Result<ListModelLks> listModelLksResult) {
                        Log.d(TAG,"this result query is "+listModelLksResult.toString());
                        ListModelLks modelLks = (ListModelLks) ((Result.Success) listModelLksResult).getData();
                        mAdapter = new ListLksAdapter(modelLks.getDataLks(), getActivity());
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                    }
                });
                return false;
            }
            String lastText;
            @Override
            public boolean onQueryTextChange(String newText) {
                if (lastText != null && lastText.length() > 1 && newText.isEmpty()) {
                    // close ctn clicked

                    Log.i(TAG, "COSSSSSSSSSSSS");
                    return true;
                }
                return false;
            }
        });
    }

    private void dummyAnimationLoading() {
        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {
                //nothing
            }

            public void onFinish() {
                mShimmerViewContainer.stopShimmerAnimation();
                recyclerView.setVisibility(View.VISIBLE);
                mShimmerViewContainer.setVisibility(View.GONE);
            }
        }.start();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}