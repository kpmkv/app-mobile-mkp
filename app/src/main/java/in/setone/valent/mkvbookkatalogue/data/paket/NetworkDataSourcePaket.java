package in.setone.valent.mkvbookkatalogue.data.paket;

import in.setone.valent.mkvbookkatalogue.data.lks.model.ListModelLks;
import in.setone.valent.mkvbookkatalogue.data.paket.model.ListModelPaket;
import in.setone.valent.mkvbookkatalogue.network.RetrofitService;
import in.setone.valent.mkvbookkatalogue.network.ServicesAPI;
import in.setone.valent.mkvbookkatalogue.network.model.ResponseLks;
import in.setone.valent.mkvbookkatalogue.network.model.ResponsePaket;
import in.setone.valent.mkvbookkatalogue.network.model.ResponseSearchLks;
import in.setone.valent.mkvbookkatalogue.network.model.ResponseSearchPaket;
import in.setone.valent.mkvbookkatalogue.root.handler.Errors;
import in.setone.valent.mkvbookkatalogue.root.handler.Result;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkDataSourcePaket {
    private ServicesAPI servicesAPI;

    //TODO:response
    public interface NetworkDataSourcePaketCallback {
        void onSuccess(Result<ListModelPaket> results);
        void onFailed(Result<ListModelPaket> errors);
    }

    public void getDataPaket(final NetworkDataSourcePaketCallback callback){
        servicesAPI = RetrofitService.createService(ServicesAPI.class);
        servicesAPI.getDataPaket().enqueue(new Callback<ResponsePaket>() {
            @Override
            public void onResponse(Call<ResponsePaket> call, Response<ResponsePaket> response) {
                if (!response.body().isStatus()){
                    callback.onFailed(new Result.Error(new Errors.ErrorMessage<>(response.body().getMassage())));
                }
                callback.onSuccess(new Result.Success<>(new ListModelPaket(response.body().getResults())));
            }

            @Override
            public void onFailure(Call<ResponsePaket> call, Throwable t) {
                callback.onFailed(new Result.Error(new Errors.ErrorThrowable(t)));
            }
        });
    }

    public void searchPaket(final NetworkDataSourcePaketCallback callback, String query){
        servicesAPI = RetrofitService.createService(ServicesAPI.class);
        servicesAPI.getQueryPaket(query).enqueue(new Callback<ResponseSearchPaket>() {
            @Override
            public void onResponse(Call<ResponseSearchPaket> call, Response<ResponseSearchPaket> response) {
                if (!response.body().isStatus()){
                    callback.onFailed(new Result.Error(new Errors.ErrorMessage<>(response.body().getMassage())));
                }
                callback.onSuccess(new Result.Success<>(new ListModelPaket(response.body().getResults())));
            }

            @Override
            public void onFailure(Call<ResponseSearchPaket> call, Throwable t) {
                callback.onFailed(new Result.Error(new Errors.ErrorThrowable(t)));
            }
        });
    }

}
