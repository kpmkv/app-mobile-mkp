package in.setone.valent.mkvbookkatalogue.data.lks;

import androidx.lifecycle.MutableLiveData;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ListModelLks;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ModelLks;
import in.setone.valent.mkvbookkatalogue.root.handler.Result;

public class RepositoryLks {

    private DataSourceLks dataSourceLks = new DataSourceLks();

    public MutableLiveData<Result<ListModelLks>> dataLks(){
        return dataSourceLks.lksLiveData();
    }

    public MutableLiveData<Result<ListModelLks>> resultSearchLks(String query){
        return dataSourceLks.searchQuery(query);
    }

}
