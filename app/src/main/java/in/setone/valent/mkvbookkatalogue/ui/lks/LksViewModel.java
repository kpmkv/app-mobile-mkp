package in.setone.valent.mkvbookkatalogue.ui.lks;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import in.setone.valent.mkvbookkatalogue.data.lks.RepositoryLks;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ListModelLks;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ModelLks;
import in.setone.valent.mkvbookkatalogue.room.AppDatabase;
import in.setone.valent.mkvbookkatalogue.room.BookDao;
import in.setone.valent.mkvbookkatalogue.root.handler.Result;

public class LksViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private RepositoryLks repositoryLks = new RepositoryLks();

    public LksViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is notifications fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }

    public MutableLiveData<Result<ListModelLks>> liveDataLks(){
        return repositoryLks.dataLks();
    }

    public MutableLiveData<Result<ListModelLks>> searchQueryLks(String query){
        return repositoryLks.resultSearchLks(query);
    }
}