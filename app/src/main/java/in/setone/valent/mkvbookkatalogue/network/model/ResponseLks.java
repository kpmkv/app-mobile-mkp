package in.setone.valent.mkvbookkatalogue.network.model;

import com.google.gson.annotations.SerializedName;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ModelLks;

import java.util.List;

public class ResponseLks {
    
    @SerializedName("status")
    private boolean status;
    @SerializedName("massage")
    private String massage;
    @SerializedName("results")
    private List<ModelLks> results;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public List<ModelLks> getResults() {
        return results;
    }

    public void setResults(List<ModelLks> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "ResponseLks{" +
                "status=" + status +
                ", massage='" + massage + '\'' +
                ", results=" + results.toString() +
                '}';
    }
}
