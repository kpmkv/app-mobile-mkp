package in.setone.valent.mkvbookkatalogue.ui.details;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.navigation.Navigation;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import in.setone.valent.mkvbookkatalogue.BuildConfig;
import in.setone.valent.mkvbookkatalogue.R;
import in.setone.valent.mkvbookkatalogue.data.lks.parcelablelks.ParcelLks;
import in.setone.valent.mkvbookkatalogue.data.paket.parcelablepaket.ParcelPaket;
import in.setone.valent.mkvbookkatalogue.room.AppDatabase;
import in.setone.valent.mkvbookkatalogue.room.BookDao;
import in.setone.valent.mkvbookkatalogue.room.model.BookModel;
import in.setone.valent.mkvbookkatalogue.ui.details.adapter.GridAdapter;
import in.setone.valent.mkvbookkatalogue.ui.lks.LksFragment;
import in.setone.valent.mkvbookkatalogue.ui.paket.PaketFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class DetailsScrollingActivity extends AppCompatActivity {

    public static String KEY_OPENED = "OPEN_DORPRAISE";

    private String TAG = "DetailsScrollingActivity";
    private CollapsingToolbarLayout collapsingToolbar;
    private int mutedColor = R.attr.colorPrimary;
    private BookModel mainData;
    private Target loadtarget;
    private Bitmap bitmap;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private AppDatabase appDatabase;
    private BookDao bookDao;
    private List<String> data = new ArrayList<>();
    private TextView namaBuku, isbn, jenis, kelasjen, semester, kurikulum, halaman, pengarang, editor, overview, terbit, penerbit;
    FloatingActionButton fab,fab2;
    private boolean isFav = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        appDatabase = AppDatabase.getDatabase(getBaseContext());
        appDatabase.getOpenHelper().getWritableDatabase();
        bookDao = appDatabase.bookDao();
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);

        namaBuku = findViewById(R.id.inNamaBuku);
        isbn = findViewById(R.id.inIsbn);
        jenis = findViewById(R.id.inJenisBuku);
        kelasjen = findViewById(R.id.inKelasJenjang);
        semester = findViewById(R.id.inSemester);
        kurikulum = findViewById(R.id.inKurikulum);
        halaman = findViewById(R.id.inJumHalaman);
        pengarang = findViewById(R.id.tvPengarang);
        editor = findViewById(R.id.tvEditor);
        overview = findViewById(R.id.tvOverview);
        terbit = findViewById(R.id.tvDiterbitkan);
        penerbit = findViewById(R.id.tvPenerbit);

        recyclerView = (RecyclerView) findViewById(R.id.rc_grid_images);
        layoutManager = new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        Intent intent = getIntent();
//        if (intent.getStringExtra(KEY_OPENED).equals("lks")){
//            ParcelLks parcel = intent.getParcelableExtra(LksFragment.KEY_SENDER_LKS);
//            mainData = new BookModel(parcel.getId(),parcel.getNama(), parcel.getIsbn(), parcel.getType(), parcel.getKategori(), parcel.getKurikulum(), parcel.getJenjang(), parcel.getKelas(), parcel.getSemester(), parcel.getJumlah_halaman(), parcel.getPengarang(), parcel.getEditor(), parcel.getOverview(), parcel.getTanggal_terbit(), parcel.getPath_foto(), parcel.getPath_daftarisi(), parcel.getBahasa(), parcel.getPenerbit());
//        }
        if (intent.getStringExtra(KEY_OPENED).equals("paket")) {
            ParcelPaket parcel = intent.getParcelableExtra(PaketFragment.KEY_SENDER_PAKET);
            mainData = new BookModel(parcel.getId(),parcel.getNama(), parcel.getIsbn(), parcel.getType(), parcel.getKategori(), parcel.getKurikulum(), parcel.getJenjang(), parcel.getKelas(), parcel.getSemester(), parcel.getJumlah_halaman(), parcel.getPengarang(), parcel.getEditor(), parcel.getOverview(), parcel.getTanggal_terbit(), parcel.getPath_foto(), parcel.getPath_daftarisi(), parcel.getBahasa(), parcel.getPenerbit());
        }

        addData();

        recyclerView.setVisibility(View.VISIBLE);
        mAdapter = new GridAdapter(data);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        namaBuku.setText(": " + mainData.getNama());
        isbn.setText(": " + mainData.getIsbn());
        jenis.setText(": " + mainData.getType());
        kelasjen.setText(": " + mainData.getKelas() + "/" + mainData.getJenjang());
        semester.setText(": " + mainData.getSemester());
        kurikulum.setText(": " + mainData.getKurikulum());
        halaman.setText(": " + mainData.getJumlah_halaman());
        pengarang.setText(mainData.getPengarang());
        editor.setText(mainData.getEditor());
        overview.setText(mainData.getOverview());
        terbit.setText("Tahun terbit : " + mainData.getTanggal_terbit());
        penerbit.setText(mainData.getPenerbit());

        setUpToolbar();

        isFavCheck();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PackageManager pm = view.getContext().getPackageManager();
                try {
                    // Raise exception if whatsapp doesn't exist
                    PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);

                    Intent waIntent = new Intent(Intent.ACTION_SEND);
                    waIntent.setType("text/plain");
                    waIntent.setPackage("com.whatsapp");
                    waIntent.putExtra(Intent.EXTRA_TEXT, "Hallo Saya ingin Pesan Buku dengan *Judul " + mainData.getNama() + "* Sejumlah ");
                    startActivity(waIntent);
                } catch (PackageManager.NameNotFoundException e) {
                    Snackbar.make(view, "Please Install Whatsapp to send massage !!!", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isFav) {
                    new deleteAsyncTask(bookDao).execute(mainData);
                    Snackbar.make(view, "Removed from favorite book", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    isFav = true;
                    changeImage(isFav);
                } else {
                    new insertAsyncTask(bookDao).execute(mainData);
                    Snackbar.make(view, "Saved to favorite book", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    isFav = false;
                    changeImage(isFav);
                }

            }
        });
    }

    private void changeImage(boolean isFav) {

        if (isFav){
            fab2.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_24dp));
        }else {
            fab2.setImageDrawable(getResources().getDrawable(R.drawable.ic_delete_24dp));
        }

    }

    private void isFavCheck() {

        try {
            isFav = new loadAsyncTask(bookDao).execute(mainData.getId()).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new CountDownTimer(1500, 1000) {

            public void onTick(long millisUntilFinished) {
                //nothing
            }

            public void onFinish() {
                changeImage(isFav);
                fab2.setVisibility(View.VISIBLE);
            }
        }.start();

    }

    private void addData() {
        data.add(mainData.getPath_foto());

        String[] parts = mainData.getPath_daftarisi().split(",");

        for (String string : parts) {
            data.add(string);
        }
    }

    private void setUpToolbar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // pengaturan dan inisialisasi collapsing toolbar
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        collapsingToolbar.setTitle(mainData.getNama());

        // inisialisasi ImageView
        ImageView header = (ImageView) findViewById(R.id.iv_header);

        // mengambil gambar bitmap yang digunakan pada image view
        final String url_image = BuildConfig.uriApi + mainData.getPath_foto();
        Picasso.get().load(url_image).into(header);

        // mengambil gambar bitmap yang digunakan pada image view
        loadBitmap(url_image);

        // mengekstrak warna dari gambar yang digunakan
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                mutedColor = palette.getMutedColor(R.attr.colorPrimary);
                collapsingToolbar.setContentScrimColor(mutedColor);
            }
        });
    }

    public void loadBitmap(String url) {

        if (loadtarget == null) loadtarget = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                // do something with the Bitmap
                handleLoadedBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }

        };

        Picasso.get().load(url).into(loadtarget);
    }

    public void handleLoadedBitmap(Bitmap b) {
        this.bitmap = b;
    }

    private static class insertAsyncTask extends AsyncTask<BookModel, Void, Void> {

        private BookDao mAsyncTaskDao;

        insertAsyncTask(BookDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final BookModel... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<BookModel, Void, Void> {

        private BookDao mAsyncTaskDao;

        deleteAsyncTask(BookDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final BookModel... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }

    private static class loadAsyncTask extends AsyncTask<Integer, Void, Boolean> {

        private BookDao mAsyncTaskDao;

        loadAsyncTask(BookDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Boolean doInBackground(Integer... integers) {
            Cursor cursor = mAsyncTaskDao.loadCursor(integers[0]);
//            Log.d("BBBBBB "+integers, String.valueOf(cursor.getInt( cursor.getColumnIndex(cursor.getColumnName(0)))));
            if(cursor!=null && cursor.getCount()>0) {
                Log.d("BLAS ", "input : "+ Arrays.toString(integers) +" rsult : null");
                return false;
            } else {
                Log.d("BLAS ", "input : "+ Arrays.toString(integers) +" rsult : yes");
                return true;
            }
        }
    }

}
