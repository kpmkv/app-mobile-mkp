package in.setone.valent.mkvbookkatalogue.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {

    static final String KEY_NAMA ="nama_perusahaan";
    static final String KEY_TLP ="telp";
    static final String KEY_WA ="weha";
    static final String KEY_EMAIL ="email";

    private static SharedPreferences getSharedPreference(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setNama(Context context, String name){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_NAMA, name);
        editor.apply();
    }
    public static String getNama(Context context){
        return getSharedPreference(context).getString(KEY_NAMA,"");
    }

    public static void setTlp(Context context, String name){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_TLP, name);
        editor.apply();
    }
    public static String getTlp(Context context){
        return getSharedPreference(context).getString(KEY_TLP,"");
    }

    public static void setWa(Context context, String name){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_WA, name);
        editor.apply();
    }
    public static String getWa(Context context){
        return getSharedPreference(context).getString(KEY_WA,"");
    }

    public static void setEmail(Context context, String name){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(KEY_EMAIL, name);
        editor.apply();
    }
    public static String getEmail(Context context){
        return getSharedPreference(context).getString(KEY_EMAIL,"");
    }

}