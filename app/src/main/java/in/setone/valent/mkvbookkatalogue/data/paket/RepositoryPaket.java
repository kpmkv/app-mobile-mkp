package in.setone.valent.mkvbookkatalogue.data.paket;

import androidx.lifecycle.MutableLiveData;
import in.setone.valent.mkvbookkatalogue.data.lks.DataSourceLks;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ListModelLks;
import in.setone.valent.mkvbookkatalogue.data.paket.model.ListModelPaket;
import in.setone.valent.mkvbookkatalogue.root.handler.Result;

public class RepositoryPaket {


    private DataSourcePaket dataSourcePaket = new DataSourcePaket();

    public MutableLiveData<Result<ListModelPaket>> dataPaket(){
        return dataSourcePaket.paketLiveData();
    }

    public MutableLiveData<Result<ListModelPaket>> resultSearchPaket(String query){
        return dataSourcePaket.searchQueryPaket(query);
    }

}
