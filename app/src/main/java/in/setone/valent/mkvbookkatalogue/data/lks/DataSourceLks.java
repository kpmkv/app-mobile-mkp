package in.setone.valent.mkvbookkatalogue.data.lks;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ListModelLks;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ModelLks;
import in.setone.valent.mkvbookkatalogue.root.handler.Result;

public class DataSourceLks {

    private NetworkDataSourceLks networkDataSourceLks = new NetworkDataSourceLks();

    public MutableLiveData<Result<ListModelLks>> lksLiveData(){
        final MutableLiveData<Result<ListModelLks>> liveData = new MutableLiveData<>();
        networkDataSourceLks.getDataLks(new NetworkDataSourceLks.NetworkDataSourceLksCallback() {
            @Override
            public void onSuccess(final Result<ListModelLks> results) {
                liveData.setValue(results);
            }

            @Override
            public void onFailed(Result<ListModelLks> errors) {
                liveData.setValue(errors);
            }
        });
        return liveData;
    }

    public MutableLiveData<Result<ListModelLks>> searchQuery(String query){
        final MutableLiveData<Result<ListModelLks>> liveData = new MutableLiveData<>();
        networkDataSourceLks.searchLks(new NetworkDataSourceLks.NetworkDataSourceLksCallback() {
            @Override
            public void onSuccess(Result<ListModelLks> results) {
                liveData.setValue(results);
            }

            @Override
            public void onFailed(Result<ListModelLks> errors) {
                liveData.setValue(errors);
            }
        }, query);
        return liveData;
    }

}
