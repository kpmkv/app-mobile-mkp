package in.setone.valent.mkvbookkatalogue.ui.lks.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import in.setone.valent.mkvbookkatalogue.BuildConfig;
import in.setone.valent.mkvbookkatalogue.ui.details.DetailsScrollingActivity;
import in.setone.valent.mkvbookkatalogue.R;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ModelLks;
import in.setone.valent.mkvbookkatalogue.data.lks.parcelablelks.ParcelLks;
import in.setone.valent.mkvbookkatalogue.ui.lks.LksFragment;

import java.util.List;

public class ListLksAdapter extends RecyclerView.Adapter<ListLksAdapter.LksViewHolder> {

    private List<ModelLks> data;
    private Activity activity;

    public ListLksAdapter(List<ModelLks> data, FragmentActivity activity){
        this.data = data;
        this.activity = activity;
    }

    @NonNull
    @Override
    public LksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new LksViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final LksViewHolder holder, int position) {

        final ModelLks singleData = data.get(position);

        //load Image Poster
        final String url_image = BuildConfig.uriApi + singleData.getPath_foto();
        Picasso.get().load(url_image).into(holder.imagePoster);

        holder.tvJudul.setText(singleData.getNama());
        holder.tvPengarang.setText(singleData.getPengarang());
        holder.tvDetails.setText("Tidak Ada Diskripsi Tentang Buku ini !!");
        if (singleData.getOverview() != null){
            holder.tvDetails.setText(singleData.getOverview());
        }
        holder.tvIsbn.setText("ISBN : "+singleData.getIsbn());
        holder.tvTerbit.setText("Diterbitkan : "+singleData.getTanggal_terbit());

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ParcelLks parcelLks = new ParcelLks(singleData);

                Intent intent = new Intent(activity, DetailsScrollingActivity.class);
                intent.putExtra(DetailsScrollingActivity.KEY_OPENED, "lks");
                intent.putExtra(LksFragment.KEY_SENDER_LKS, parcelLks);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class LksViewHolder extends RecyclerView.ViewHolder {

        public ImageView imagePoster;
        public TextView tvJudul,tvPengarang,tvDetails,tvIsbn,tvTerbit;
        public ConstraintLayout parent;

        public LksViewHolder(@NonNull View itemView) {
            super(itemView);
            imagePoster = itemView.findViewById(R.id.imagePlacheholdView);
            tvJudul = itemView.findViewById(R.id.textPlacheholdName);
            tvPengarang = itemView.findViewById(R.id.textPlacheholdPengarang);
            tvDetails = itemView.findViewById(R.id.textDiskripsiSingkat);
            tvIsbn = itemView.findViewById(R.id.textPlacheholdIsbn);
            tvTerbit = itemView.findViewById(R.id.textPlacheholdTerbit);
            parent = itemView.findViewById(R.id.parent_item_list_placehold);
        }
    }
}
