package in.setone.valent.mkvbookkatalogue.network.model;

import com.google.gson.annotations.SerializedName;
import in.setone.valent.mkvbookkatalogue.data.paket.model.ModelPaket;

import java.util.List;

public class ResponsePaket {

    @SerializedName("status")
    private boolean status;
    @SerializedName("massage")
    private String massage;
    @SerializedName("results")
    private List<ModelPaket> results;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }

    public List<ModelPaket> getResults() {
        return results;
    }

    public void setResults(List<ModelPaket> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "ResponsePaket{" +
                "status=" + status +
                ", massage='" + massage + '\'' +
                ", results=" + results.toString() +
                '}';
    }

}
