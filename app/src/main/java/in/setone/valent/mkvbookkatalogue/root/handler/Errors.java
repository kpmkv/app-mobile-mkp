package in.setone.valent.mkvbookkatalogue.root.handler;

public class Errors {
    private Errors() {
    }

    @Override
    public String toString() {
        if (this instanceof Errors.ErrorMessage) {
            Errors.ErrorMessage errorMessage = (Errors.ErrorMessage) this;
            return "ErrorMessage[data=" + errorMessage.getErrorMessage().toString()+ "]";
        } else if (this instanceof Errors.ErrorThrowable) {
            Errors.ErrorThrowable errorThrowable = (Errors.ErrorThrowable) this;
            return "ErrorThrowable[exception=" + errorThrowable.getErrorThrowable().getMessage() + "]";
        }
        return "";
    }

    // Success sub-class
    public final static class ErrorMessage<T> extends Errors {
        private T data;

        public ErrorMessage(T data) {
            this.data = data;
        }

        public T getErrorMessage() {
            return this.data;
        }
    }

    // Error sub-class
    public final static class ErrorThrowable extends Errors {
        private Throwable error;

        public ErrorThrowable(Throwable error) {
            this.error = error;
        }

        public Throwable getErrorThrowable() {
            return this.error;
        }
    }
}
