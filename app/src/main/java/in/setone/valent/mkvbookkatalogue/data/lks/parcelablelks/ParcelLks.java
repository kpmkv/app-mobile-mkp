package in.setone.valent.mkvbookkatalogue.data.lks.parcelablelks;

import android.os.Parcel;
import android.os.Parcelable;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ModelLks;

public class ParcelLks extends ModelLks implements Parcelable {
    public ParcelLks(Parcel in) {
        super(
                in.readInt(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readInt(),
                in.readInt(),
                in.readInt(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString(),
                in.readString());

//        this.setId(in.readInt());
//        this.setNama(in.readString());
//        this.setIsbn(in.readString());
//        this.setType(in.readString());
//        this.setKategori(in.readString());
//        this.setKurikulum(in.readString());
//        this.setJenjang(in.readString());
//        this.setKelas(in.readInt());
//        this.setSemester(in.readInt());
//        this.setJumlah_halaman(in.readInt());
//        this.setPengarang(in.readString());
//        this.setEditor(in.readString());
//        this.setOverview(in.readString());
//        this.setTanggal_terbit(in.readString());
//        this.setPath_foto(in.readString());
//        this.setPath_daftarisi(in.readString());
//        this.setBahasa(in.readString());
//        this.setPenerbit(in.readString());
    }

    public static final Creator<ParcelLks> CREATOR = new Creator<ParcelLks>() {
        @Override
        public ParcelLks createFromParcel(Parcel in) {
            return new ParcelLks(in);
        }

        @Override
        public ParcelLks[] newArray(int size) {
            return new ParcelLks[size];
        }
    };

    public ParcelLks(ModelLks singleData) {
        super(
                singleData.getId(),
                singleData.getNama(),
                singleData.getIsbn(),
                singleData.getType(),
                singleData.getKategori(),
                singleData.getKurikulum(),
                singleData.getJenjang(),
                singleData.getKelas(),
                singleData.getSemester(),
                singleData.getJumlah_halaman(),
                singleData.getPengarang(),
                singleData.getEditor(),
                singleData.getOverview(),
                singleData.getTanggal_terbit(),
                singleData.getPath_foto(),
                singleData.getPath_daftarisi(),
                singleData.getBahasa(),
                singleData.getPenerbit());

//        this.setId(singleData.getId());
//        this.setNama(singleData.getNama());
//        this.setIsbn(singleData.getIsbn());
//        this.setType(singleData.getType());
//        this.setKategori(singleData.getKategori());
//        this.setKurikulum(singleData.getKurikulum());
//        this.setJenjang(singleData.getJenjang());
//        this.setKelas(singleData.getKelas());
//        this.setSemester(singleData.getSemester());
//        this.setJumlah_halaman(singleData.getJumlah_halaman());
//        this.setPengarang(singleData.getPengarang());
//        this.setEditor(singleData.getEditor());
//        this.setOverview(singleData.getOverview());
//        this.setTanggal_terbit(singleData.getTanggal_terbit());
//        this.setPath_foto(singleData.getPath_foto());
//        this.setPath_daftarisi(singleData.getPath_daftarisi());
//        this.setBahasa(singleData.getBahasa());
//        this.setPenerbit(singleData.getPenerbit());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.getId());
        dest.writeString(this.getNama());
        dest.writeString(this.getIsbn());
        dest.writeString(this.getType());
        dest.writeString(this.getKategori());
        dest.writeString(this.getKurikulum());
        dest.writeString(this.getJenjang());
        dest.writeInt(this.getKelas());
        dest.writeInt(this.getSemester());
        dest.writeInt(this.getJumlah_halaman());
        dest.writeString(this.getPengarang());
        dest.writeString(this.getEditor());
        dest.writeString(this.getOverview());
        dest.writeString(this.getTanggal_terbit());
        dest.writeString(this.getPath_foto());
        dest.writeString(this.getPath_daftarisi());
        dest.writeString(this.getBahasa());
        dest.writeString(this.getPenerbit());
    }
}
