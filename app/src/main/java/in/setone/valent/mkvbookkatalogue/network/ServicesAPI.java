package in.setone.valent.mkvbookkatalogue.network;


import in.setone.valent.mkvbookkatalogue.network.model.ResponseLks;
import in.setone.valent.mkvbookkatalogue.network.model.ResponsePaket;
import in.setone.valent.mkvbookkatalogue.network.model.ResponseSearchLks;
import in.setone.valent.mkvbookkatalogue.network.model.ResponseSearchPaket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ServicesAPI {

    @GET("lks")
    Call<ResponseLks> getDataLks();

    @GET("paket")
    Call<ResponsePaket> getDataPaket();

    @GET("lks/search")
    Call<ResponseSearchLks> getQueryLks(@Query("query") String query);

    @GET("lks/paket")
    Call<ResponseSearchPaket> getQueryPaket(@Query("query") String query);
}
