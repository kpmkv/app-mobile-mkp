package in.setone.valent.mkvbookkatalogue.ui.paket;

import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.*;
import android.widget.SearchView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.shimmer.ShimmerFrameLayout;
import in.setone.valent.mkvbookkatalogue.R;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ListModelLks;
import in.setone.valent.mkvbookkatalogue.data.paket.model.ListModelPaket;
import in.setone.valent.mkvbookkatalogue.root.handler.Result;
import in.setone.valent.mkvbookkatalogue.ui.lks.LksViewModel;
import in.setone.valent.mkvbookkatalogue.ui.lks.adapter.ListLksAdapter;
import in.setone.valent.mkvbookkatalogue.ui.paket.adapter.ListPaketAdapter;

public class PaketFragment extends Fragment {

    public static String KEY_SENDER_PAKET = "SENDER_PAKET";

    private PaketViewModel paketViewModel;
    private String TAG = "PaketFargment";
    private RecyclerView recyclerView;

    private RecyclerView.Adapter mAdapter;

    private RecyclerView.LayoutManager layoutManager;

    private ShimmerFrameLayout mShimmerViewContainer;

    private SearchView searchView;


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        inflater.inflate(R.menu.options_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.search);
        searchView = (SearchView) MenuItemCompat.getActionView(menuItem);

        onSearchQuery();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        paketViewModel =
                ViewModelProviders.of(this).get(PaketViewModel.class);
        View root = inflater.inflate(R.layout.fragment_paket, container, false);
        mShimmerViewContainer = root.findViewById(R.id.shimmer_view_container_paket);
        recyclerView = root.findViewById(R.id.list_paket);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        mShimmerViewContainer.startShimmerAnimation();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);


        dummyAnimationLoading();

        getDataLks();

    }

    private void getDataLks() {
        paketViewModel.liveDataPaket().observe(getActivity(), new Observer<Result<ListModelPaket>>() {
            @Override
            public void onChanged(Result<ListModelPaket> result) {
                ListModelPaket modelPaket = (ListModelPaket) ((Result.Success) result).getData();
                mAdapter = new ListPaketAdapter(modelPaket.getDataPaket(), getActivity());
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    private void onSearchQuery() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                paketViewModel.searchQueryPaket(query).observe(getActivity(), new Observer<Result<ListModelPaket>>() {
                    @Override
                    public void onChanged(Result<ListModelPaket> listModelPaketResult) {
                        Log.d(TAG,"this result query is "+listModelPaketResult.toString());
                        ListModelPaket modelPaket = (ListModelPaket) ((Result.Success) listModelPaketResult).getData();
                        mAdapter = new ListPaketAdapter(modelPaket.getDataPaket(), getActivity());
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                    }
                });
                return false;
            }
            String lastText;
            @Override
            public boolean onQueryTextChange(String newText) {
                if (lastText != null && lastText.length() > 1 && newText.isEmpty()) {
                    // close ctn clicked

                    Log.i(TAG, "COSSSSSSSSSSSS");
                    return true;
                }
                return false;
            }
        });
    }

    private void dummyAnimationLoading() {
        new CountDownTimer(5000, 1000) {

            public void onTick(long millisUntilFinished) {
                //nothing
            }

            public void onFinish() {
                mShimmerViewContainer.stopShimmerAnimation();
                recyclerView.setVisibility(View.VISIBLE);
                mShimmerViewContainer.setVisibility(View.GONE);
            }
        }.start();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}