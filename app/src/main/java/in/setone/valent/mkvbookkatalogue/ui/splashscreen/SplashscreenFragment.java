package in.setone.valent.mkvbookkatalogue.ui.splashscreen;

import android.annotation.SuppressLint;
import android.os.CountDownTimer;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;
import in.setone.valent.mkvbookkatalogue.R;
import in.setone.valent.mkvbookkatalogue.customwidget.GifImageView;

import java.util.Objects;

public class SplashscreenFragment extends Fragment {

    private SplashscreenViewModel mViewModel;

    public static SplashscreenFragment newInstance() {
        return new SplashscreenFragment();
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
//        ConstraintLayout constraintLayout = getActivity().findViewById(R.id.container);
//        constraintLayout.setBackgroundColor(R.color.white);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_splashscreen, container, false);

        new CountDownTimer(4000, 1000) {

            public void onTick(long millisUntilFinished) {
                //nothing
            }

            public void onFinish() {
                Navigation.findNavController(rootView).navigate(R.id.action_splashscreenFragment_to_navigation_home);

            }
        }.start();

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(SplashscreenViewModel.class);
        // TODO: Use the ViewModel
    }

}
