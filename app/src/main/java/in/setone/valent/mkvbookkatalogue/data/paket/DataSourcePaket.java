package in.setone.valent.mkvbookkatalogue.data.paket;

import androidx.lifecycle.MutableLiveData;
import in.setone.valent.mkvbookkatalogue.data.lks.NetworkDataSourceLks;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ListModelLks;
import in.setone.valent.mkvbookkatalogue.data.paket.model.ListModelPaket;
import in.setone.valent.mkvbookkatalogue.root.handler.Result;

public class DataSourcePaket {

    private NetworkDataSourcePaket networkDataSourcePaket = new NetworkDataSourcePaket();

    public MutableLiveData<Result<ListModelPaket>> paketLiveData(){
        final MutableLiveData<Result<ListModelPaket>> liveData = new MutableLiveData<>();
        networkDataSourcePaket.getDataPaket(new NetworkDataSourcePaket.NetworkDataSourcePaketCallback(){
            @Override
            public void onSuccess(final Result<ListModelPaket> results) {
                liveData.setValue(results);
            }

            @Override
            public void onFailed(Result<ListModelPaket> errors) {
                liveData.setValue(errors);
            }
        });
        return liveData;
    }

    public MutableLiveData<Result<ListModelPaket>> searchQueryPaket(String query){
        final MutableLiveData<Result<ListModelPaket>> liveData = new MutableLiveData<>();
        networkDataSourcePaket.searchPaket(new NetworkDataSourcePaket.NetworkDataSourcePaketCallback() {
            @Override
            public void onSuccess(Result<ListModelPaket> results) {
                liveData.setValue(results);
            }

            @Override
            public void onFailed(Result<ListModelPaket> errors) {
                liveData.setValue(errors);
            }
        }, query);
        return liveData;
    }
}
