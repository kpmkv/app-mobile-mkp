package in.setone.valent.mkvbookkatalogue.data.paket.model;

import java.util.List;

public class ListModelPaket {
    private List<ModelPaket> paketList;

    public ListModelPaket(List<ModelPaket> modelPaketList) {
        this.paketList = modelPaketList;
    }

    public List<ModelPaket> getDataPaket() {
        return paketList;
    }

    public void setDataPaket(List<ModelPaket> modelPaketList) {
        this.paketList = modelPaketList;
    }

    @Override
    public String toString() {
        return "ListModelLks{" +
                "listOfDataLks=" + paketList +
                '}';
    }
}
