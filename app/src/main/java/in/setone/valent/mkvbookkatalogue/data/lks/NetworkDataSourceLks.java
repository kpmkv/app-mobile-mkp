package in.setone.valent.mkvbookkatalogue.data.lks;

import android.widget.Toast;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ListModelLks;
import in.setone.valent.mkvbookkatalogue.data.lks.model.ModelLks;
import in.setone.valent.mkvbookkatalogue.network.RetrofitService;
import in.setone.valent.mkvbookkatalogue.network.ServicesAPI;
import in.setone.valent.mkvbookkatalogue.network.model.ResponseLks;
import in.setone.valent.mkvbookkatalogue.network.model.ResponseSearchLks;
import in.setone.valent.mkvbookkatalogue.root.handler.Errors;
import in.setone.valent.mkvbookkatalogue.root.handler.Result;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkDataSourceLks {
    private ServicesAPI servicesAPI;

    //TODO:response
    public interface NetworkDataSourceLksCallback {
        void onSuccess(Result<ListModelLks> results);
        void onFailed(Result<ListModelLks> errors);
    }

    public void getDataLks(final NetworkDataSourceLksCallback callback){
        servicesAPI = RetrofitService.createService(ServicesAPI.class);
        servicesAPI.getDataLks().enqueue(new Callback<ResponseLks>() {
            @Override
            public void onResponse(Call<ResponseLks> call, Response<ResponseLks> response) {
                if (response.isSuccessful()){
                    if (!response.body().isStatus()){
                        callback.onFailed(new Result.Error(new Errors.ErrorMessage<>(response.body().getMassage())));
                    }
                    callback.onSuccess(new Result.Success<>(new ListModelLks(response.body().getResults())));
                }
            }

            @Override
            public void onFailure(Call<ResponseLks> call, Throwable t) {
                callback.onFailed(new Result.Error(new Errors.ErrorThrowable(t)));
            }
        });
    }

    public void searchLks(final NetworkDataSourceLksCallback callback, String query){
        servicesAPI = RetrofitService.createService(ServicesAPI.class);
        servicesAPI.getQueryLks(query).enqueue(new Callback<ResponseSearchLks>() {
            @Override
            public void onResponse(Call<ResponseSearchLks> call, Response<ResponseSearchLks> response) {
                if (!response.body().isStatus()){
                    callback.onFailed(new Result.Error(new Errors.ErrorMessage<>(response.body().getMassage())));
                }
                callback.onSuccess(new Result.Success<>(new ListModelLks(response.body().getResults())));
            }

            @Override
            public void onFailure(Call<ResponseSearchLks> call, Throwable t) {
                callback.onFailed(new Result.Error(new Errors.ErrorThrowable(t)));
            }
        });
    }

}
